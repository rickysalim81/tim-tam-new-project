import * as types from "../types";

const initialState = {
  user: "Unknown",
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case types.GET_USER:
      return {
        user: action.payload,
      };
    case types.GET_GOOGLE_USER:
      return {
        user: action.payload,
      };

    default:
      return state;
  }
};
