import * as types from "../types";
import Axios from "axios";
import { push } from "react-router-redux";

export function userLogin(
  email,
  password,
  router,
  setShow,
  setResponseMessage
) {
  return (dispatch) => {
    return Axios.post("http://localhost:8000/login", {
      email: email,
      password: password,
    }).then((res) => {
      if (!res.data.auth) {
        console.log(res);
        setResponseMessage(res.data);
        setShow(true);
      } else {
        dispatch(
          {
            type: types.GET_USER,
            payload: res.data.user,
          },
          push("/")
        );
        localStorage.setItem("token", res.data.token);
        router.push("/");
      }
    });
  };
}

export function userGoogleLogin(res, router, setShow, setResponseMessage) {
  return (dispatch) => {
    if (!res.data.auth) {
      console.log(res);
      setResponseMessage(res.data);
      setShow(true);
    } else {
      dispatch(
        {
          type: types.GET_GOOGLE_USER,
          payload: res.data.user,
        },
        push("/")
      );
      localStorage.setItem("token", res.data.token);
      router.push("/");
    }
  };
}
