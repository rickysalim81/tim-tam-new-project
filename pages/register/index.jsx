import Head from "next/head";
import React, { useState, useEffect } from "react";
import {
  Button,
  Card,
  Col,
  Container,
  Form,
  Navbar,
  Row,
  Modal,
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import useInput from "../../hooks/useInput";
import Axios from "axios";
import Link from "next/link";
import { useRouter } from "next/router";
import { CheckLoginToken } from "../../lib/CheckToken";

export default function Login() {
  const router = useRouter();

  useEffect(() => {
    CheckLoginToken(router);
  }, []);

  const email = useInput("");
  const fullname = useInput("");
  const password = useInput("");
  const phone = useInput("");
  const address = useInput("");

  const [gender, setGender] = useState("");

  const [show, setShow] = useState(false);
  const [responseMessage, setResponseMessage] = useState("");

  const [validated, setValidated] = useState(false);

  const handleSubmit = (event) => {
    event.preventDefault();

    const form = event.currentTarget;
    if (form.checkValidity() === false) {
      event.stopPropagation();
    }
    setValidated(true);
    if (form.checkValidity() === true) {
      const user = {
        email: email.value,
        password: password.value,
        full_name: fullname.value,
        gender: gender,
        address: address.value,
        phone: phone.value,
      };
      Axios.post("http://localhost:8000/register", user)
        .then((res) => {
          setResponseMessage(res.data);
          setShow(true);
        })
        .catch((err) => {
          setResponseMessage(err.response.data);
          setShow(true);
        });
    }
  };

  const handleClose = () => setShow(false);

  return (
    <div>
      <Head>
        <title>Register Page</title>
        <meta name="description" content="Register page" />
      </Head>
      <header>
        <Navbar expand="lg" style={{ marginBottom: "5rem" }}>
          <Container>
            <Navbar.Brand href="/landing-page">HOME</Navbar.Brand>
          </Container>
        </Navbar>
      </header>
      <main>
        {" "}
        <Container>
          <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
          >
            <Modal.Header closeButton>
              <Modal.Title>{responseMessage}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              {responseMessage === "Register Success"
                ? "Thank you for your registration"
                : "Try with another email"}
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              {responseMessage === "Register Success" ? (
                <Link href="/login">
                  <Button variant="primary" type="submit">
                    Login
                  </Button>
                </Link>
              ) : (
                ""
              )}
            </Modal.Footer>
          </Modal>
          <Row>
            <Col md={{ span: 6, offset: 3 }}>
              <Card style={{ marginTop: "3rem" }}>
                <Card.Body>
                  <Card.Title>REGISTER</Card.Title>
                  <Card.Text>
                    <Form
                      noValidate
                      validated={validated}
                      onSubmit={handleSubmit}
                    >
                      <Form.Group className="mb-3">
                        <Form.Label>Full Name</Form.Label>
                        <Form.Control
                          type="text"
                          required
                          placeholder="Enter full name"
                          {...fullname}
                        />
                      </Form.Group>

                      <Form.Group className="mb-3">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                          type="email"
                          required
                          placeholder="Enter email"
                          {...email}
                        />
                      </Form.Group>

                      <Form.Group className="mb-3">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                          type="password"
                          required
                          placeholder="Password"
                          {...password}
                        />
                      </Form.Group>

                      <Form.Group className="mb-3">
                        <Form.Label>Phone</Form.Label>
                        <Form.Control
                          type="number"
                          placeholder="Enter phone"
                          {...phone}
                          required
                        />
                      </Form.Group>

                      <Form.Group
                        onChange={(e) => setGender(e.target.value)}
                        className="mb-3"
                      >
                        <Form.Label as="legend">Gender</Form.Label>
                        <div key={`inline-radio}`} className="mb-3">
                          <Form.Check
                            required
                            name="formGender"
                            id="formGender1"
                            inline
                            type="radio"
                            label="Male"
                            value="male"
                          />

                          <Form.Check
                            name="formGender"
                            id="formGender2"
                            inline
                            type="radio"
                            label="Female"
                            value="female"
                          />
                        </div>
                      </Form.Group>

                      <Form.Group className="mb-3">
                        <Form.Label>Address</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Enter address"
                          {...address}
                          required
                        />
                      </Form.Group>

                      <div className="d-grid gap-2">
                        <Button type="submit" variant="primary" size="md">
                          Register
                        </Button>
                      </div>
                    </Form>
                  </Card.Text>
                  <p>
                    I already have account.
                    <Link href="/login">
                      <Card.Link>Login</Card.Link>
                    </Link>
                  </p>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </main>
    </div>
  );
}
