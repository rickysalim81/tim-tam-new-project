import React, { useEffect } from "react";
import {
    Button,
    Card,
    Col,
    Container,
    Nav,
    Navbar,
    Row,
} from "react-bootstrap";
import axios from "axios";
import Image from "next/image";
import Link from "next/link";
import Logo from "../../public/assets/logo_anamnesia_200.png";
import "bootstrap/dist/css/bootstrap.min.css";

export const getStaticPaths = async () => {
    const res = await fetch("http://localhost:8000/api/landingpage/listnews");
    const data = await res.json();
    const paths = data.news.map((data) => {
        return {
            params: { id: data.id.toString() },
        };
    });
    if (data.isFallBack) {
        return <div>Loading...</div>;
    }
    return {
        paths,
        fallback: false,
    };
};

export const getStaticProps = async (context) => {
    const id = {
        id: context.params.id,
    };
    const res = await axios.post(
        "http://localhost:8000/api/landingpage/detailnews",
        id
    );
    const data = await res.data.news;

    return {
        props: { detailList: data },
    };
};

const DetailLandingNews = ({ detailList }) => {
    useEffect(async () => {
        const data = {
            id: detailList.id,
            viewer: detailList.viewer,
        };
        await axios
            .post("http://localhost:8000/api/landingpage/viewnews", data, {
                headers: { "Content-Type": "application/json" },
            })
            .then((res) => {});
    }, []);

    async function handleLike() {
        const data = {
            id: detailList.id,
            like: detailList.like,
        };
        await axios
            .post("http://localhost:8000/api/landingpage/likenews", data, {
                headers: { "Content-Type": "application/json" },
            })
            .then((res) => {
                if (res.status === 200) {
                    location.reload();
                }
            });
    }
    return (
        <>
            <Navbar
                expand="lg"
                variant="light"
                style={{ marginBottom: "5rem" }}
            >
                <Container>
                    <Navbar.Brand href="#home">
                        <Image src={Logo} />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse className="justify-content-end">
                        <Nav>
                            <Nav.Link href="#home">Login</Nav.Link>
                            <Nav.Link href="#link">Register</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            <Container fluid="md" style={{ marginBottom: "2rem" }}>
                <Row>
                    <Col>
                        <Link href="/landing-page">
                            <Button variant="secondary">Back</Button>
                        </Link>
                    </Col>
                </Row>
            </Container>
            <Container fluid="md" style={{ marginBottom: "5rem" }}>
                <Row style={{ marginBottom: "2rem" }}>
                    <Col>
                        <Card style={{ width: "100%" }}>
                            <Card.Body>
                                <Card.Title>{detailList.headline}</Card.Title>
                                <Card.Subtitle className="mb-2 text-muted">
                                    {detailList.categoriesId.category_name}
                                </Card.Subtitle>
                                <Card.Text>
                                    <div
                                        dangerouslySetInnerHTML={{
                                            __html: `${detailList.body}`,
                                        }}
                                    />
                                </Card.Text>
                                <Card.Text>
                                    <svg
                                        onClick={handleLike}
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="32"
                                        height="32"
                                        fill="currentColor"
                                        className="bi bi-hand-thumbs-up"
                                        viewBox="0 0 16 16"
                                    >
                                        <path d="M8.864.046C7.908-.193 7.02.53 6.956 1.466c-.072 1.051-.23 2.016-.428 2.59-.125.36-.479 1.013-1.04 1.639-.557.623-1.282 1.178-2.131 1.41C2.685 7.288 2 7.87 2 8.72v4.001c0 .845.682 1.464 1.448 1.545 1.07.114 1.564.415 2.068.723l.048.03c.272.165.578.348.97.484.397.136.861.217 1.466.217h3.5c.937 0 1.599-.477 1.934-1.064a1.86 1.86 0 0 0 .254-.912c0-.152-.023-.312-.077-.464.201-.263.38-.578.488-.901.11-.33.172-.762.004-1.149.069-.13.12-.269.159-.403.077-.27.113-.568.113-.857 0-.288-.036-.585-.113-.856a2.144 2.144 0 0 0-.138-.362 1.9 1.9 0 0 0 .234-1.734c-.206-.592-.682-1.1-1.2-1.272-.847-.282-1.803-.276-2.516-.211a9.84 9.84 0 0 0-.443.05 9.365 9.365 0 0 0-.062-4.509A1.38 1.38 0 0 0 9.125.111L8.864.046zM11.5 14.721H8c-.51 0-.863-.069-1.14-.164-.281-.097-.506-.228-.776-.393l-.04-.024c-.555-.339-1.198-.731-2.49-.868-.333-.036-.554-.29-.554-.55V8.72c0-.254.226-.543.62-.65 1.095-.3 1.977-.996 2.614-1.708.635-.71 1.064-1.475 1.238-1.978.243-.7.407-1.768.482-2.85.025-.362.36-.594.667-.518l.262.066c.16.04.258.143.288.255a8.34 8.34 0 0 1-.145 4.725.5.5 0 0 0 .595.644l.003-.001.014-.003.058-.014a8.908 8.908 0 0 1 1.036-.157c.663-.06 1.457-.054 2.11.164.175.058.45.3.57.65.107.308.087.67-.266 1.022l-.353.353.353.354c.043.043.105.141.154.315.048.167.075.37.075.581 0 .212-.027.414-.075.582-.05.174-.111.272-.154.315l-.353.353.353.354c.047.047.109.177.005.488a2.224 2.224 0 0 1-.505.805l-.353.353.353.354c.006.005.041.05.041.17a.866.866 0 0 1-.121.416c-.165.288-.503.56-1.066.56z" />
                                    </svg>
                                    <p>{detailList.like}</p>
                                    <svg
                                        xmlns="http://www.w3.org/2000/svg"
                                        width="32"
                                        height="32"
                                        fill="currentColor"
                                        className="bi bi-eye"
                                        viewBox="0 0 16 16"
                                    >
                                        <path d="M16 8s-3-5.5-8-5.5S0 8 0 8s3 5.5 8 5.5S16 8 16 8zM1.173 8a13.133 13.133 0 0 1 1.66-2.043C4.12 4.668 5.88 3.5 8 3.5c2.12 0 3.879 1.168 5.168 2.457A13.133 13.133 0 0 1 14.828 8c-.058.087-.122.183-.195.288-.335.48-.83 1.12-1.465 1.755C11.879 11.332 10.119 12.5 8 12.5c-2.12 0-3.879-1.168-5.168-2.457A13.134 13.134 0 0 1 1.172 8z" />
                                        <path d="M8 5.5a2.5 2.5 0 1 0 0 5 2.5 2.5 0 0 0 0-5zM4.5 8a3.5 3.5 0 1 1 7 0 3.5 3.5 0 0 1-7 0z" />
                                    </svg>
                                    <p>{detailList.viewer}</p>
                                </Card.Text>
                            </Card.Body>
                        </Card>
                    </Col>
                </Row>
            </Container>
            <footer
                className="page-footer font-small blue pt-4"
                style={{ backgroundColor: "#d4d4d4" }}
            >
                <div className="container-fluid text-center text-md-left">
                    <div className="row justify-content-md-center">
                        <div className="col-md-6 mt-md-0 mt-3">
                            <Image src={Logo} />
                        </div>
                        <hr className="clearfix w-100 d-md-none pb-0" />
                    </div>
                </div>

                <div className="footer-copyright text-center py-3">
                    © 2021 Copyright Tim Tam
                </div>
            </footer>
        </>
    );
};

export default DetailLandingNews;
