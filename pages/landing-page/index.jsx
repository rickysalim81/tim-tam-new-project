import React, { useState, useEffect } from "react";
import { Card, Col, Container, Nav, Navbar, Row } from "react-bootstrap";
import axios from "axios";
import Image from "next/image";
import Link from "next/link";
import Logo from "../../public/assets/logo_anamnesia_200.png";
import Moment from "react-moment";
import "bootstrap/dist/css/bootstrap.min.css";

export default function LandingPage() {
    axios.defaults.withCredentials = true;
    const [listNews, setListNews] = useState([]);
    // const [viewer, plusView] = useState(0);

    useEffect(async () => {
        await axios
            .get("http://localhost:8000/api/landingpage/listnews")
            .then((res) => {
                if (res.status === 200) {
                    setListNews(res.data.news);
                }
            });
    }, []);

    return (
        <>
            <Navbar
                expand="lg"
                variant="light"
                style={{ marginBottom: "5rem" }}
            >
                <Container>
                    <Navbar.Brand href="#home">
                        <Image src={Logo} />
                    </Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    {/* <Navbar.Collapse id="basic-navbar-nav">
                        <Nav className="me-auto">
                            <Nav.Link href="#home">Sport</Nav.Link>
                            <Nav.Link href="#link">Technology</Nav.Link>
                        </Nav>
                    </Navbar.Collapse> */}
                    <Navbar.Collapse className="justify-content-end">
                        <Nav>
                            <Nav.Link href="/login">Login</Nav.Link>
                            <Nav.Link href="/register">Register</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            <Container fluid="md" style={{ marginBottom: "5rem" }}>
                {listNews.map((data) => {
                    return (
                        <Row style={{ marginBottom: "2rem" }}>
                            <Col>
                                <Link
                                    href={`/detail-landing-page/${encodeURIComponent(
                                        data.id
                                    )}`}
                                >
                                    <Card style={{ width: "100%" }}>
                                        <Card.Body>
                                            <Card.Title>
                                                {data.headline}
                                            </Card.Title>
                                            <Card.Subtitle className="mb-2 text-muted">
                                                {
                                                    data.categoriesId
                                                        .category_name
                                                }
                                            </Card.Subtitle>
                                            <Card.Text>
                                                <Moment format="YYYY/MM/DD">
                                                    {data.createdAt}
                                                </Moment>
                                            </Card.Text>
                                        </Card.Body>
                                    </Card>
                                </Link>
                            </Col>
                        </Row>
                    );
                })}
            </Container>
            <footer
                className="page-footer font-small blue pt-4"
                style={{ backgroundColor: "#d4d4d4" }}
            >
                <div className="container-fluid text-center text-md-left">
                    <div className="row justify-content-md-center">
                        <div className="col-md-6 mt-md-0 mt-3">
                            <Image src={Logo} />
                            {/* <h5 className="text-uppercase">Footer Content</h5>
                            <p>
                                Here you can use rows and columns to organize
                                your footer content.
                            </p> */}
                        </div>

                        <hr className="clearfix w-100 d-md-none pb-0" />

                        {/* <div className="col-md-3 mb-md-0 mb-3">
                            <h5 className="text-uppercase">Links</h5>
                            <ul className="list-unstyled">
                                <li>
                                    <a href="#!">Link 1</a>
                                </li>
                                <li>
                                    <a href="#!">Link 2</a>
                                </li>
                                <li>
                                    <a href="#!">Link 3</a>
                                </li>
                                <li>
                                    <a href="#!">Link 4</a>
                                </li>
                            </ul>
                        </div>

                        <div className="col-md-3 mb-md-0 mb-3">
                            <h5 className="text-uppercase">Links</h5>
                            <ul className="list-unstyled">
                                <li>
                                    <a href="#!">Link 1</a>
                                </li>
                                <li>
                                    <a href="#!">Link 2</a>
                                </li>
                                <li>
                                    <a href="#!">Link 3</a>
                                </li>
                                <li>
                                    <a href="#!">Link 4</a>
                                </li>
                            </ul>
                        </div> */}
                    </div>
                </div>

                <div className="footer-copyright text-center py-3">
                    © 2021 Copyright Tim Tam
                </div>
            </footer>
        </>
    );
}
