import Head from "next/head";
import React, { useState, useEffect } from "react";
import {
  Button,
  Card,
  Col,
  Container,
  Form,
  Navbar,
  Row,
  Modal,
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import styles from "./login.module.css";
import { useRouter } from "next/router";
import { useDispatch } from "react-redux";
import { userGoogleLogin, userLogin } from "../../store/actions/userActions";
import Link from "next/link";
import { CheckLoginToken } from "../../lib/CheckToken";
import GoogleLogin from "react-google-login";
import Axios from "axios";

export default function Login() {
  const router = useRouter();

  useEffect(() => {
    CheckLoginToken(router);
  }, []);

  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [responseMessage, setResponseMessage] = useState("");
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);

  const dispatch = useDispatch();

  const login = () => {
    dispatch(userLogin(email, password, router, setShow, setResponseMessage));
  };

  const responseSuccessGoogle = (res) => {
    console.log(res);
    Axios({
      method: "POST",
      url: "http://localhost:8000/login/googlelogin",
      data: { tokenId: res.tokenId },
    }).then((res) => {
      dispatch(userGoogleLogin(res, router, setShow, setResponseMessage));
      console.log("Google Login Succes", res.data);
    });
  };

  const responseErrorGoogle = (res) => {};

  return (
    <div>
      <Head>
        <title>Login Page</title>
        <meta name="description" content="Login page" />
      </Head>
      <header>
        <Navbar expand="lg" style={{ marginBottom: "5rem" }}>
          <Container>
            <Navbar.Brand href="/landing-page">HOME</Navbar.Brand>
          </Container>
        </Navbar>
      </header>
      <main>
        <Container>
          <Modal
            show={show}
            onHide={handleClose}
            backdrop="static"
            keyboard={false}
          >
            <Modal.Header closeButton>
              <Modal.Title>{responseMessage}</Modal.Title>
            </Modal.Header>
            <Modal.Body>
              {responseMessage === "Invalid Username & Password !!"
                ? "Your email or password is incorrect"
                : ""}
            </Modal.Body>
            <Modal.Footer>
              <Button variant="secondary" onClick={handleClose}>
                Close
              </Button>
              {responseMessage === "Register Success" ? (
                <Link href="/login">
                  <Button variant="primary" type="submit">
                    Login
                  </Button>
                </Link>
              ) : (
                ""
              )}
            </Modal.Footer>
          </Modal>
          <Row className="justify-content-md-center">
            <Col md={{ span: 5 }}>
              <Card className={styles.card}>
                <Card.Body className={styles.cardBody}>
                  <Card.Text>
                    <Form style={{ marginTop: "10%" }}>
                      <Form.Group className="mb-3">
                        <Form.Label>Email</Form.Label>
                        <Form.Control
                          id="email"
                          type="email"
                          placeholder="Enter email"
                          onChange={(e) => setEmail(e.target.value)}
                        />
                      </Form.Group>

                      <Form.Group className="mb-3">
                        <Form.Label>Password</Form.Label>
                        <Form.Control
                          id="password"
                          type="password"
                          placeholder="Password"
                          onChange={(e) => setPassword(e.target.value)}
                        />
                      </Form.Group>

                      <div style={{ textAlign: "center", marginTop: "50px" }}>
                        <Button onClick={login} variant="primary" size="md">
                          Login
                        </Button>
                      </div>
                    </Form>
                  </Card.Text>
                  <div style={{ textAlign: "center" }}>
                    <p>
                      Dont Have account ?<Link href="/register">Register</Link>
                    </p>
                  </div>
                  <GoogleLogin
                    clientId="888114490069-2h64h0hrlf09avovfl0b2qch6nggresk.apps.googleusercontent.com"
                    buttonText="Login With Google"
                    onSuccess={responseSuccessGoogle}
                    onFailure={responseErrorGoogle}
                    cookiePolicy={"single_host_origin"}
                  />
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </main>
    </div>
  );
}
