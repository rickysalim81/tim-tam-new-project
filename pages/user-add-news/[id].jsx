import React, { useEffect, useState } from "react";
import {
    Alert,
    Button,
    Col,
    Container,
    Form,
    Nav,
    Navbar,
    Row,
} from "react-bootstrap";
import { useRouter } from "next/router";
import axios from "axios";
import Editor from "./editor";
import Link from "next/link";
import "bootstrap/dist/css/bootstrap.min.css";

export const getStaticPaths = async () => {
    const res = await fetch("http://localhost:8000/api/news/list-news");
    const data = await res.json();

    const paths = data.news.map((data) => {
        return {
            params: { id: data.id.toString() },
        };
    });

    if (data.isFallBack) {
        return <div>Loading...</div>;
    }

    return {
        paths,
        fallback: true,
    };
};

export const getStaticProps = async (context) => {
    const id = {
        id: context.params.id,
    };
    const res = await axios.post(
        "http://localhost:8000/api/news/detail-news",
        id
    );
    const data = await res.data.news;

    return {
        props: { dataNews: data },
    };
};

const UserEditNews = ({ dataNews }) => {
    const router = useRouter();
    const [editorLoaded, setEditorLoaded] = useState(false);
    const [category, setCategory] = useState([]);
    const [headline, setHeadline] = useState("");
    const [categoriesId, setCategoriesId] = useState("");
    const [body, setBody] = useState("");
    const [usersId, setUsersID] = useState("");

    const [infoAlert, setInfoAlert] = useState(false);
    const [messageAlert, setMessageAlert] = useState("");
    const [variant, setVariant] = useState("");

    useEffect(async () => {
        setEditorLoaded(true);

        axios
            .get("http://localhost:8000/api/category/list-category")
            .then((res) => {
                if (res.status === 200) {
                    setCategory(res.data.categories);
                }
            });

        setHeadline(dataNews.headline);
        setBody(dataNews.body);
        setCategoriesId(dataNews.categories_id);
    }, []);

    async function handleDraft() {
        if (headline !== "" || body !== "" || categoriesId !== "") {
            const data = {
                headline: headline !== "" ? headline : null,
                body: body !== "" ? body : null,
                categoriesId: categoriesId !== 0 ? categoriesId : null,
                // users_id: users_id,
            };

            await axios
                .post("http://localhost:8000/api/news/draft-news", data, {
                    headers: {
                        "Content-Type": "application/json",
                        // Authorization: token,
                    },
                })
                .then(() => {
                    setInfoAlert(true);
                    setMessageAlert("Save to draft success!");
                    setVariant("info");

                    setTimeout(() => {
                        setInfoAlert(false);
                        setMessageAlert("");
                        setVariant("");
                    }, 3000);
                });

            router.push("/user-news");
        } else {
            setInfoAlert(true);
            setMessageAlert("Input must be filled!");
            setVariant("danger");

            setTimeout(() => {
                setInfoAlert(false);
                setMessageAlert("");
                setVariant("");
            }, 3000);
        }
    }

    async function handleOnSubmit(event) {
        event.preventDefault();
        const data = {
            headline: headline,
            body: body,
            categories_id: categoriesId,
            // users_id: usersId != "" ? usersId : "",
        };
        await axios
            .post("http://localhost:8000/api/news/create-news", data, {
                headers: {
                    "Content-Type": "application/json",
                    // Authorization: token,
                },
            })
            .then((res) => {
                if (res.status === 200) {
                    router.push("/user-news");
                }
            });
    }

    return (
        <>
            <Navbar expand="lg" style={{ marginBottom: "5rem" }}>
                <Container>
                    <Navbar.Brand href="#home">WELCOME.</Navbar.Brand>
                    <Navbar.Toggle aria-controls="basic-navbar-nav" />
                    <Navbar.Collapse className="justify-content-end">
                        <Nav>
                            <Nav.Link href="#home">Profile</Nav.Link>
                            <Nav.Link href="#link">Logout</Nav.Link>
                        </Nav>
                    </Navbar.Collapse>
                </Container>
            </Navbar>
            <Container>
                {infoAlert && <Alert variant={variant}>{messageAlert}</Alert>}
            </Container>
            <Container fluid="md" style={{ marginBottom: "5rem" }}>
                <Row style={{ marginBottom: "2rem" }}>
                    <Col md={4}>
                        <Link href="/user-news">
                            <Button variant="secondary">Back</Button>
                        </Link>
                    </Col>
                    <Col
                        md={{ span: 4, offset: 4 }}
                        style={{ textAlign: "right" }}
                    >
                        {/* <Link href="/user-news"> */}
                        <Button variant="warning" onClick={handleDraft}>
                            Save to Draft
                        </Button>
                        {/* </Link> */}
                    </Col>
                </Row>
                <Form onSubmit={handleOnSubmit}>
                    <Form.Group className="mb-3">
                        <Form.Label>Headline</Form.Label>
                        <Form.Control
                            type="text"
                            placeholder="Enter headline"
                            name="headline"
                            onChange={(e) => setHeadline(e.target.value)}
                            value={headline}
                        />
                    </Form.Group>

                    <Form.Group className="mb-3">
                        <Form.Label>Category</Form.Label>
                        <Form.Select
                            aria-label="Default select example"
                            name="categoriesId"
                            onChange={(e) => setCategoriesId(e.target.value)}
                            value={categoriesId}
                        >
                            <option>Select the category</option>
                            {category.map((data) => {
                                return (
                                    <option value={data.id}>
                                        {data.category_name}
                                    </option>
                                );
                            })}
                        </Form.Select>
                    </Form.Group>

                    <Editor
                        name="body"
                        editorLoaded={editorLoaded}
                        onChange={(data) => {
                            setBody(data);
                        }}
                        value={body}
                    />

                    <div className="d-grid gap-2">
                        <Button
                            variant="primary"
                            type="submit"
                            className="mt-3"
                        >
                            Submit
                        </Button>
                    </div>
                </Form>
            </Container>
        </>
    );
};

export default UserEditNews;
