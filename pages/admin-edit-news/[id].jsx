import React, { useEffect, useState } from "react";
import {
  Alert,
  Button,
  Col,
  Container,
  Form,
  Nav,
  Navbar,
  Row,
} from "react-bootstrap";
import { useRouter } from "next/router";
import axios from "axios";
import Editor from "./editor";
import Link from "next/link";
import "bootstrap/dist/css/bootstrap.min.css";

export const getStaticPaths = async () => {
  const res = await fetch("http://localhost:8000/api/news/list-news");
  const data = await res.json();

  const paths = data.news.map((data) => {
    return {
      params: { id: data.id.toString() },
    };
  });

  if (data.isFallBack) {
    return <div>Loading...</div>;
  }

  return {
    paths,
    fallback: true,
  };
};

export const getStaticProps = async (context) => {
  const id = {
    id: context.params.id,
  };
  const res = await axios.post(
    "http://localhost:8000/api/news/detail-news",
    id
  );
  const data = await res.data.news;

  return {
    props: { dataNews: data },
  };
};

const AdminEditNews = ({ dataNews }) => {
  const router = useRouter();
  const [editorLoaded, setEditorLoaded] = useState(false);
  const [category, setCategory] = useState([]);
  const [headline, setHeadline] = useState("");
  const [categoriesId, setCategoriesId] = useState("");
  const [body, setBody] = useState("");
  const [AdminId, setAdminID] = useState("");

  const [infoAlert, setInfoAlert] = useState(false);
  const [messageAlert, setMessageAlert] = useState("");
  const [variant, setVariant] = useState("");

  useEffect(async () => {
    setEditorLoaded(true);

    axios
      .get("http://localhost:8000/api/category/list-category")
      .then((res) => {
        if (res.status === 200) {
          setCategory(res.data.categories);
        }
      });

    setHeadline(dataNews.headline);
    setBody(dataNews.body);
    setCategoriesId(dataNews.categories_id);
  }, []);

  async function handleOnApprove(event) {
    event.preventDefault();
    const data = {
      headline: headline,
      body: body,
      categories_id: categoriesId,
    };
    await axios
      .post("http://localhost:8000/api/news/create-news", data, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then((res) => {
        if (res.status === 200) {
          router.push("/admin-news");
        }
      });
  }

  return (
    <>
      <Navbar expand="lg" style={{ marginBottom: "5rem" }}>
        <Container>
          <Navbar.Brand href="#home">WELCOME.</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse className="justify-content-end">
            <Nav>
              <Nav.Link href="#home">Profile</Nav.Link>
              <Nav.Link href="#link">Logout</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <Container>
        {infoAlert && <Alert variant={variant}>{messageAlert}</Alert>}
      </Container>
      <Container fluid="md" style={{ marginBottom: "5rem" }}>
        <Row style={{ marginBottom: "2rem" }}>
          <Col md={4}>
            <Link href="/admin-news">
              <Button variant="secondary">Back</Button>
            </Link>
          </Col>
        </Row>
        <Form onApprove={handleOnApprove}>
          <Form.Group className="mb-3">
            <Form.Label>Headline</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter headline"
              name="headline"
              onChange={(e) => setHeadline(e.target.value)}
              value={headline}
            />
          </Form.Group>

          <Form.Group className="mb-3">
            <Form.Label>Category</Form.Label>
            <Form.Select
              aria-label="Default select example"
              name="categoriesId"
              onChange={(e) => setCategoriesId(e.target.value)}
              value={categoriesId}
            >
              <option>Select the category</option>
              {category.map((data) => {
                return <option value={data.id}>{data.category_name}</option>;
              })}
            </Form.Select>
          </Form.Group>

          <Editor
            name="body"
            editorLoaded={editorLoaded}
            onChange={(data) => {
              setBody(data);
            }}
            value={body}
          />

          <div className="d-grid gap-2">
            <Button variant="primary" type="approve" className="mt-3">
              Approve
            </Button>
          </div>
        </Form>
      </Container>
    </>
  );
};

export default AdminEditNews;
