import React, { useEffect, useState } from "react";
import {
  Alert,
  Button,
  Col,
  Container,
  Form,
  Nav,
  Navbar,
  Row,
} from "react-bootstrap";
import { useRouter } from "next/router";
import axios from "axios";
import Editor from "./editor";
import Link from "next/link";
import "bootstrap/dist/css/bootstrap.min.css";

export default function AdminEditNews() {
  const router = useRouter();
  const [editorLoaded, setEditorLoaded] = useState(false);
  const [category, setCategory] = useState([]);
  const [headline, setHeadline] = useState("");
  const [categoriesId, setCategoriesId] = useState("");
  const [body, setBody] = useState("");
  const [adminId, setAdminID] = useState("");

  const [infoAlert, setInfoAlert] = useState(false);
  const [messageAlert, setMessageAlert] = useState("");
  const [variant, setVariant] = useState("");

  useEffect(() => {
    setEditorLoaded(true);

    axios
      .get("http://localhost:8000/api/category/list-category")
      .then((res) => {
        if (res.status === 200) {
          setCategory(res.data.categories);
        }
      });
  }, []);

  function handleFieldOnChange(event) {
    this.setState({
      [event.target.name]: event.target.value,
    });
  }

  async function handleOnApprove(event) {
    event.preventDefault();
    const data = {
      headline: headline,
      body: body,
      categories_id: categoriesId,
      users_id: usersId,
    };
    await axios
      .post("/api/news/update-news", data, {
        headers: {
          "Content-Type": "application/json",
          Authorization: this.state.token,
        },
      })
      .then(() => {
        // this.setState({
        //     redirectPage: true,
        // });
      });
    // handleRedirectPage();
  }

  return (
    <>
      <Navbar expand="lg" style={{ marginBottom: "5rem" }}>
        <Container>
          <Navbar.Brand href="#home">News From User</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse className="justify-content-end">
            <Nav>
              <Nav.Link href="#link">Logout</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <Container>
        {infoAlert && <Alert variant={variant}>{messageAlert}</Alert>}
      </Container>
      <Container fluid="md" style={{ marginBottom: "5rem" }}>
        <Row style={{ marginBottom: "2rem" }}>
          <Col md={4}>
            <Link href="/admin-news">
              <Button variant="secondary">Back</Button>
            </Link>
          </Col>
        </Row>
        <Form onApprove={handleOnApprove}>
          <Form.Group className="mb-3">
            <Form.Label>Title</Form.Label>
            <Form.Control
              type="text"
              placeholder="Enter title"
              name="headline"
              onChange={(e) => setHeadline(e.target.value)}
            />
          </Form.Group>

          <Form.Group className="mb-3">
            <Form.Label>Category</Form.Label>
            <Form.Select
              aria-label="Default select example"
              name="categoriesId"
              onChange={(e) => setCategoriesId(e.target.value)}
            >
              <option>Select the category</option>
              {category.map((data) => {
                return <option value={data.id}>{data.category_name}</option>;
              })}
            </Form.Select>
          </Form.Group>

          <Editor
            name="body"
            onChange={(data) => {
              setBody(data);
            }}
            editorLoaded={editorLoaded}
          />

          <div className="d-grid gap-1">
            <Button variant="success" type="approve" className="mt-4">
              Approve
            </Button>
          </div>
        </Form>
      </Container>
    </>
  );
}
