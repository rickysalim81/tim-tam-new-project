import React, { useEffect, useState } from "react";
import {
  Button,
  Col,
  Container,
  Nav,
  Navbar,
  Row,
  Table,
} from "react-bootstrap";
import axios from "axios";
import Link from "next/link";
import "bootstrap/dist/css/bootstrap.min.css";

export default function AdminNews() {
  axios.defaults.withCredentials = true;
  const [news, setNews] = useState([]);

  useEffect(() => {
    axios.get("http://localhost:8000/api/news/list-news").then((res) => {
      if (res.status === 200) {
        setNews(res.data.news);
      }
    });
  }, []);

  async function handlePublish(event) {
    const data = {
      id: event.target.value,
    };

    await axios
      .post("http://localhost:8000/api/news/approve-news", data, {
        headers: {
          "Content-Type": "application/json",
        },
      })
      .then(() => {
        window.location.reload(false);
      });
  }

  return (
    <>
      <Navbar expand="lg" style={{ marginBottom: "5rem" }}>
        <Container>
          <Navbar.Brand href="#home">Admin</Navbar.Brand>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse className="justify-content-end">
            <Nav>
              <Nav.Link href="/logout">Logout</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <Container fluid="md" style={{ marginBottom: "5rem" }}>
        <Row style={{ marginBottom: "2rem" }}>
          <Col md={{ span: 4, offset: 8 }} style={{ textAlign: "right" }}>
            <Link href="/admin-add-category">
              <Button variant="primary">Add Category</Button>
            </Link>
          </Col>
        </Row>
        <Table striped bordered hover>
          <thead>
            <tr>
              <th>No</th>
              <th>News</th>
              {/* <th>Category</th> */}
              <th>Body</th>
              <th>Viewer</th>
              <th>Like</th>
              <th>Actions</th>
            </tr>
          </thead>
          <tbody>
            {news.map((data, index) => {
              return (
                  <tr>
                      <td>{index + 1}</td>
                      <td>{data.headline}</td>
                      <td>{data.body}</td>
                      <td>{data.viewer}</td>
                      <td>{data.like}</td>
                      <td>
                          {data.status === "0" && (
                              <Link
                                  href={`/admin-edit-news/${encodeURIComponent(
                                      data.id
                                  )}`}
                              >
                                  <Button
                                      variant="success"
                                      size="sm"
                                      value={data.id}
                                  >
                                      Edit
                                  </Button>
                              </Link>
                          )}
                          {data.status === "1" && (
                              <Button
                                  variant="primary"
                                  onClick={handlePublish}
                                  size="sm"
                                  value={data.id}
                              >
                                  Approve
                              </Button>
                          )}
                          {data.status === "2" && (
                              <Button variant="success" size="sm" disabled>
                                  Approve
                              </Button>
                          )}
                      </td>
                  </tr>
              );
            })}
          </tbody>
        </Table>
      </Container>
    </>
  );
}
