import Head from "next/head";
import React, { useState, useEffect } from "react";
import {
  Button,
  Card,
  Col,
  Container,
  Form,
  Navbar,
  Row,
  Modal,
} from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";
import { useSelector } from "react-redux";
import { CheckValidToken } from "../../lib/CheckToken";
import { useRouter } from "next/router";

export default function Profile() {
  const router = useRouter();

  useEffect(() => {
    CheckValidToken(router);
  }, []);
  const { user } = useSelector((state) => state.user);
  return (
    <div>
      <Head>
        <title>Your Profile</title>
        <meta name="description" content="Profile page" />
      </Head>
      <header>
        <Navbar expand="lg" style={{ marginBottom: "5rem" }}>
          <Container>
            <Navbar.Brand href="/landing-page">Home</Navbar.Brand>
          </Container>
        </Navbar>
      </header>
      <main>
        {" "}
        <Container>
          <Row>
            <Col md={{ span: 6, offset: 3 }}>
              <Card style={{ marginTop: "3rem" }}>
                <Card.Body>
                  <Card.Title>Profile</Card.Title>
                  <Card.Text>
                    <Form>
                      <Form.Group className="mb-3">
                        <Form.Label>Full Name</Form.Label>
                        <Form.Control
                          type="text"
                          required
                          value={user.full_name}
                          disabled
                          placeholder="Enter full name"
                        />
                      </Form.Group>

                      <Form.Group className="mb-3">
                        <Form.Label>Email address</Form.Label>
                        <Form.Control
                          type="email"
                          required
                          value={user.email}
                          disabled
                          placeholder="Enter email"
                        />
                      </Form.Group>

                      <Form.Group className="mb-3">
                        <Form.Label>Phone</Form.Label>
                        <Form.Control
                          type="number"
                          placeholder="Enter phone"
                          value={user.phone}
                          disabled
                          required
                        />
                      </Form.Group>

                      <Form.Group className="mb-3">
                        <Form.Label>Address</Form.Label>
                        <Form.Control
                          type="text"
                          placeholder="Enter address"
                          value={user.address}
                          disabled
                          required
                        />
                      </Form.Group>

                      <div className="">
                        <Button type="submit" variant="primary" size="md">
                          Edit
                        </Button>
                      </div>
                    </Form>
                  </Card.Text>
                </Card.Body>
              </Card>
            </Col>
          </Row>
        </Container>
      </main>
    </div>
  );
}
