import Axios from "axios";

export function CheckLoginToken(router) {
  const token = localStorage.getItem("token");

  if (token) {
    Axios.get("http://localhost:8000/login/token", {
      headers: {
        Authorization: token,
      },
    })
      .then((res) => {
        console.log(res);
        router.push("/");
      })
      .catch((err) => {
        console.log(err);
      });
  }
}

export function CheckValidToken(router) {
  const token = localStorage.getItem("token");

  if (!token) {
    router.push("/");
  }

  if (token) {
    Axios.get("http://localhost:8000/login/token", {
      headers: {
        Authorization: token,
      },
    })
      .then((res) => {
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
        router.push("/");
      });
  }
}
